<?php
/***************************************************************
 *  Copyright notice
 *  (c) 2013 Georg Ringer <typo3@ringerge.org>
 *  All rights reserved
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Hook to display verbose information about pi1 plugin in Web>Page module
 *
 * @package TYPO3
 * @subpackage tx_newslistitems
 */
class Tx_Newslistitems_Hooks_CmsLayout extends \Tx_News_Hooks_CmsLayout {

	/**
	 * Extension key
	 *
	 * @var string
	 */
	const KEY = 'newslistitems';

	/**
	 * Path to the locallang file
	 *
	 * @var string
	 */
	const LLPATH2 = 'LLL:EXT:newslistitems/Resources/Private/Language/locallang_be.xml:';

	/**
	 * Returns information about this extension's plugin
	 *
	 * @param array $params Parameters to the hook
	 * @param mixed $pObj A reference to calling object
	 * @return string Information about pi1 plugin
	 */
	public function getExtensionSummary(array $params, \TYPO3\CMS\Backend\View\PageLayoutView $pObj) {
		$result = '';

		if ($this->showExtensionTitle()) {
			$result .= '<strong>' . $GLOBALS['LANG']->sL(self::LLPATH2 . 'plugin_title', TRUE) . '</strong>';
		}

		if ($params['row']['list_type'] == self::KEY . '_list') {
			$this->flexformData = \t3lib_div::xml2array($params['row']['pi_flexform']);

			$this->getItems();

			$result .= $this->renderSettingsAsTable($params['row']);
			$GLOBALS['SOBE']->doc->getPageRenderer()->loadScriptaculous('effects,dragdrop');
			$GLOBALS['SOBE']->doc->getPageRenderer()->addJsFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath(self::KEY) . 'Resources/Public/JavaScript/action.js');
			$GLOBALS['SOBE']->doc->getPageRenderer()->addCssFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath(self::KEY) . 'Resources/Public/Css/styles.css');
		}

		return $result;
	}

	/**
	 * Get all items set in the plugin
	 *
	 * @return void
	 */
	protected function getItems() {
		$items = $this->getFieldFromFlexform($this->flexformData, 'settings.items');
		$itemList = \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $items, TRUE);

		foreach ($itemList as $item) {
			$this->tableData[$item] = array($this->renderSingleNewsItem($item));
		}
	}

	/**
	 * Render a single news item for the preview
	 *
	 * @param integer $uid news uid
	 * @return string
	 */
	protected function renderSingleNewsItem($uid) {
		$content = '';

		$newsRecord = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('*', 'tx_news_domain_model_news', 'deleted=0 AND uid=' . $uid);

		if (is_array($newsRecord)) {
			$iconNews = t3lib_iconWorks::getSpriteIconForRecord('tx_news_domain_model_news', $newsRecord, array('title' => 'Uid: ' . $newsRecord['uid']));

			$onClickNews = $GLOBALS['SOBE']->doc->wrapClickMenuOnIcon($iconNews, 'tx_news_domain_model_news', $newsRecord['uid'], 1, '', '+info,edit', TRUE);

			$content = '<a href="#" onclick="' . htmlspecialchars($onClickNews) . '">' .
				$iconNews . htmlspecialchars(t3lib_BEfunc::getRecordTitle('tx_news_domain_model_news', $newsRecord)) .
				'</a>';
		} else {
			/** @var $message t3lib_FlashMessage */
			$text = sprintf($GLOBALS['LANG']->sL(self::LLPATH . 'pagemodule.newsNotAvailable', TRUE), $newsRecord);
			$message = t3lib_div::makeInstance('t3lib_FlashMessage', $text, '', t3lib_FlashMessage::WARNING);
			$content = $message->render();
		}

		return $content;
	}

	/**
	 * Render the settings as table
	 *
	 * @param array $record content element record
	 * @return string
	 */
	protected function renderSettingsAsTable(array $record) {
		$content = '';
		if (count($this->tableData) == 0) {
			return $content;
		}

		$visible = ($record['hidden'] == 0);
		$i = 0;
		foreach ($this->tableData as $id => $line) {
//			// Check if the setting is in the list of disabled ones
//			$class = ($i++ % 2 === 0) ? 'bgColor4' : 'bgColor3';
//			$renderedLine = '';
//
//			if (!empty($line[1])) {
//				$renderedLine = '<td style="' . ($visible ? 'font-weight:bold;' : '') . 'width:40%;">' . $line[0] . '</td>
//								<td>' . $line[1] . '</td>';
//			} else {
//				$renderedLine = '<td style="' . ($visible ? 'font-weight:bold;' : '') . '" colspan="2">' . $line[0] . '</td>';
//			}
//			$content .= '<tr class="' . ($visible ? $class : '') . '">' . $renderedLine . '</tr>';
			$content .= '<li id="el_' . $id . '">' . $line[0] . '<span class="drag">X</span></li>';
		}

		if (!empty($content)) {
			$content = '<ul id="newslistitems-list">' . $content . '</ul><span id="newslistitem-id" style="display:none">' . $record['uid'] . '</span>';
		}

		return $content;
	}

}


if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/news/Classes/Hooks/CmsLayout.php']) {
	require_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/news/Classes/Hooks/CmsLayout.php']);
}

?>