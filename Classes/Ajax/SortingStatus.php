<?php

class Tx_Newslistitems_Ajax_SortingStatus {


	/**
	 * Saves the sorting order of tasks in the backend user's uc
	 *
	 * @param array $params Array of parameters from the AJAX interface, currently unused
	 * @param \TYPO3\CMS\Core\Http\AjaxRequestHandler $ajaxObj Object of type AjaxRequestHandler
	 * @return void
	 */
	public function saveSortingState(array $params, \TYPO3\CMS\Core\Http\AjaxRequestHandler $ajaxObj) {
		$sort = array();
		$items = explode('&', \TYPO3\CMS\Core\Utility\GeneralUtility::_POST('data'));it
		$contentId = (int)\TYPO3\CMS\Core\Utility\GeneralUtility::_POST('contentID');
		foreach ($items as $item) {
			$sort[] = (int)substr($item, 21);
		}

		if ($contentId > 0 && !empty($sort)) {
			/** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
			$db = $GLOBALS['TYPO3_DB'];
			$record = $db->exec_SELECTgetSingleRow('*', 'tt_content', 'uid=' . $contentId);
			$xmlArray = t3lib_div::xml2array($record['pi_flexform']);
			$xmlArray['data']['sDEF']['lDEF']['settings.items']['vDEF'] = implode(',', $sort);
			$flexformTools = t3lib_div::makeInstance('t3lib_flexformtools');

			$GLOBALS['TYPO3_DB']->exec_UPDATEquery('tt_content', 'uid=' . $record['uid'], array(
				'pi_flexform' => $flexformTools->flexArray2Xml($xmlArray)
			));
		}
	}

}