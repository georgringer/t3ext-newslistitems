<?php
/***************************************************************
 *  Copyright notice
 *  (c) 2013 Georg Ringer <typo3@ringerge.org>
 *  All rights reserved
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace GeorgRinger\Newslistitems\Controller;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Controller of news records
 *
 * @package TYPO3
 * @subpackage tx_newslistitems
 */
class ListController extends \Tx_News_Controller_NewsController {

	/**
	 * Get all given news items
	 *
	 * @return void
	 */
	public function listAction() {
		$news = array();
		$idList = GeneralUtility::intExplode(',', $this->settings['items'], TRUE);
		foreach ($idList as $id) {
			$newsItem = $this->newsRepository->findByUid($id);
			if (!is_null($newsItem)) {
				echo $id . 'm';
				$news[] = $newsItem;
			}
		}

		$this->view->assign('news', $news);
	}

}