<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

/***************
 * Plugin
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'List',
	'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_be.xml:plugin_title'
);

$extensionName = t3lib_div::underscoredToUpperCamelCase($_EXTKEY);
$pluginSignature = strtolower($extensionName) . '_list';

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
t3lib_extMgm::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform.xml');

/***************
 * Wizard pi1
 */
if (TYPO3_MODE == 'BE') {
	$TBE_MODULES_EXT['xMOD_db_new_content_el']['addElClasses'][$pluginSignature . '_wizicon'] =
		t3lib_extMgm::extPath($_EXTKEY) . 'Resources/Private/Php/class.' . $_EXTKEY . '_wizicon.php';
}


$GLOBALS['TYPO3_CONF_VARS']['BE']['AJAX']['Newslistitems::saveSortingState'] = 'EXT:' . $_EXTKEY . '/Classes/Ajax/SortingStatus.php:Tx_Newslistitems_Ajax_SortingStatus->saveSortingState';

?>