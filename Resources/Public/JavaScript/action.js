Event.observe(document, "dom:loaded", function () {
	var changeEffect;
	Sortable.create("newslistitems-list", { handles: $$("#newslistitems-list .drag"), tag: "li", ghosting: true, overlap: "vertical", constraint: false,
		onUpdate: function (list) {
			new Ajax.Request("../../../ajax.php", {
				method: "post",
				parameters: {ajaxID: "Newslistitems::saveSortingState", data: Sortable.serialize(list), contentID:document.getElementById("newslistitem-id").innerHTML}
			});
		}
	});
});
